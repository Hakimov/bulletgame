#include "enemy.h"

extern Game *game;

Enemy::Enemy(QObject *parent) : QObject(parent), QGraphicsPixmapItem() {
    // drew the rect
    //setRect(0,0,40,40);
    setPixmap(QPixmap(":/res/enemy.png"));

    //set random position
    int random_number = rand() % (540 - 150);
    setPos(random_number, 0 - 200);

    // connect
    QTimer * timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));

    int rand_time = (rand() % 5) + 3;
    qDebug() << rand_time;
    timer->start(rand_time);
}

void Enemy::move() {
    QList<QGraphicsItem *> colliding_items = collidingItems();
    for (int i = 0, n = colliding_items.size(); i < n; ++i)     {
        if ((typeid(*(colliding_items[i])) == typeid(Player)) && (collize_flag == false))         {
            game->health->decrease();
            collize_flag = true;

            //scene()->removeItem(colliding_items[i]);
            scene()->removeItem(this);

            // delete them both
            //delete colliding_items[i];
            delete this;
            return;
        }
    }

    // move enemy down
    setPos(x(), y() + 2);
    if (pos().y() - pixmap().height() > scene()->height()) {
        scene()->removeItem(this);
        qDebug() << "enemy delete";
        delete this;
    }
}
