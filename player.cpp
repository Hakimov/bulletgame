#include "player.h"

Player::Player(QGraphicsPixmapItem *parent)
{
#if 0
    bulletsound = new QMediaPlayer;
    bulletsound->setMedia(QUrl("qrc:/res/fire.wav"));
#endif
}

void Player::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Left) {
        setPos(x() - 20, y());
        if(x() < 0){
            setPos(0, y());
        }
    }
    else
        if (event->key() == Qt::Key_Right) {
            setPos(x() + 20, y());
            if (x() > (540 - 140)) {
                setPos((540 - 140), y());
            }
        }
        else
            if (event->key() == Qt::Key_Up) {
                setPos(x(), y() - 20);
                if(y() < 0){
                    setPos(x(), 0);
                }
            }
            else
                if (event->key() == Qt::Key_Down) {
                    setPos(x(), y() + 20);
                    if(y() > (960 - 100)){
                        setPos(x(), (960 - 100));
                    }
                }
                else
                    if (event->key() == Qt::Key_Space) {
#if 0
                        //bulletsound->play();
#endif
                        Bullet *bullet = new Bullet;
                        bullet->setPos(x() + 62, y());
                        scene()->addItem(bullet);
                    }
}

void Player::spawn() {
    // create an enemy
    Enemy * enemy = new Enemy();
    scene()->addItem(enemy);
}

void Player::make_stars() {
    Stars *stars = new Stars();
    scene()->addItem(stars);
}

