#ifndef BULLET_H
#define BULLET_H

#include <QObject>
#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QTimer>
#include <QList>
#include <score.h>
#include "enemy.h"
#include <QDebug>
#include <game.h>
#include <typeinfo>

class Bullet : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit Bullet();

signals:

public slots:
    void move();
};

#endif // BULLET_H
