#include "health.h"
#include <QDebug>

Health::Health(QGraphicsItem *parent): QGraphicsTextItem(parent) {
    health = 3;
    // draw the text
    setPlainText(QString("Health: ") + QString::number(health)); // Score: 0
    setDefaultTextColor(Qt::blue);
    setFont(QFont("times", 16));
}

void Health::decrease() {
    health--;
    qDebug() << health;
    setPlainText(QString("Health: ") + QString::number(health)); // Score: 1
}

int Health::getHealth() {
    return  health;
}
