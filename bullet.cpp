#include "bullet.h"

extern Game *game;

Bullet::Bullet() {
    // drew the rect
    //setRect(0, 0, 5, 10);
    setPixmap(QPixmap(":/res/bullet.png"));

    // connect
    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));
    timer->start(15);
}

void Bullet::move()
{
    QList<QGraphicsItem *> colliding_items = collidingItems();

    for (int i = 0, n = colliding_items.size(); i < n; ++i) {
        if (typeid(*(colliding_items[i])) == typeid(Enemy)) {
            game->score->increase();
            // remove them both
            scene()->removeItem(colliding_items[i]);
            scene()->removeItem(this);
            // delete them both
            delete colliding_items[i];
            delete this;
            return;
        }
    }

    setPos(x(),y() - 20);
    if (pos().y() + pixmap().height() < 0) {
        scene()->removeItem(this);
        delete this;
        qDebug() << "bullet delete";
    }
}
