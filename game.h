#ifndef GAME_H
#define GAME_H

#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QTimer>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QObject>
#include <QGraphicsTextItem>
#include <QFont>
#include <QIcon>
//#include <QMediaPlayer>
#include <score.h>
#include <health.h>
#include <stars.h>
#include "player.h"


class Game : public QGraphicsView
{
    Q_OBJECT
public:
    explicit Game(QWidget *parent = nullptr);
    Score *score;
    Health *health;
    QGraphicsScene *scene;
    // Player *player;
    QGraphicsView *view;
    QGraphicsRectItem *backgroundRect;
    Stars *slowstars;
    //QMediaPlayer *bgndmusic;

    //void keyPressEvent(QKeyEvent *event);

private:

};

#endif // GAME_H
