#ifndef ENEMY_H
#define ENEMY_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QTimer>
#include <game.h>
#include <QList>
#include <QDebug>
#include <stdlib.h>

class Enemy : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit Enemy(QObject *parent = nullptr);

signals:

public slots:
    void move();

private:
    bool collize_flag = false;
};

#endif // ENEMY_H
