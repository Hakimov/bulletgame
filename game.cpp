#include "game.h"

Game::Game(QWidget *parent) {
    // Set icon of application
    QWidget::setWindowIcon(QIcon(":/res/icon.png"));

    // Создаем сцену
    scene = new QGraphicsScene();
    scene->setSceneRect(0, 0, 540, 960);

    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(540, 960);

    QPen penBlack(Qt::black); // Задаём чёрную кисть
    QBrush brush(QColor::fromRgb(0, 20, 20, 255));
    backgroundRect = new QGraphicsRectItem;
    backgroundRect->setBrush(brush);
    backgroundRect->setRect(0, 0, 540, 960);
    scene->addItem(backgroundRect);

    // Создаем прямоугольник
    Player *player = new Player();
    //player->setRect(0,0,30,30);
    player->setPixmap(QPixmap(":/res/player.png"));

    // добавляем его на сцену
    scene->addItem(player);

    // make rect focusable
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFlag(QGraphicsItem::ItemIsMovable);
    //player->setFlag(QGraphicsItem::ItemIsSelectable);
    player->setFocus();


    // add a view to visualize the scene
    view = new QGraphicsView(scene);

    //player->setPos(view->width()/2,view->height() - player->rect().height());
    player->setPos(view->width()/3, view->width());

    // spawn enemies
    QTimer *timer = new QTimer();
    QObject::connect(timer, SIGNAL(timeout()), player, SLOT(spawn()));
    timer->start(2000);

    QTimer *StarsTimer1 = new QTimer();
    QObject::connect(StarsTimer1, SIGNAL(timeout()), player, SLOT(make_stars()));
    StarsTimer1->start(200);

    // create the score/health
    score = new Score();
    scene->addItem(score);
    health = new Health();
    health->setPos(health->x(),health->y()+25);
    scene->addItem(health);

#if 0
    // bcgnd music
    bgndmusic = new QMediaPlayer;
    bgndmusic->setMedia(QUrl("qrc:/res/Black.mp3"));
    bgndmusic->setVolume(70);
    bgndmusic->play();
#endif
}

