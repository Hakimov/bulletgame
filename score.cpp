#include "score.h"
#include <QDebug>

Score::Score(QGraphicsItem *parent): QGraphicsTextItem(parent) {
    score = 0;
    // draw the text
    setPlainText(QString("Score: ") + QString::number(score)); // Score: 0
    setDefaultTextColor(Qt::blue);
    setFont(QFont("times", 16));
}

void Score::increase() {
    score++;
    //qDebug() << score;
    setPlainText(QString("Score: ") + QString::number(score)); // Score: 1
}

int Score::getScore() {
    return  score;
}
