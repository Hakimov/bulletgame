#-------------------------------------------------
#
# Project created by QtCreator 2017-06-18T11:37:33
#
#-------------------------------------------------

QT      += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtChessRev
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp \
    game.cpp \
    bullet.cpp \
    enemy.cpp \
    score.cpp \
    health.cpp \
    stars.cpp \
    player.cpp

HEADERS  += \
    game.h \
    bullet.h \
    enemy.h \
    score.h \
    health.h \
    stars.h \
    player.h

FORMS    +=


RESOURCES += \
    res/res.qrc


#LIBS = $(SUBLIBS) -L/home/lele/Qt5.1.1/5.1.1/gcc/lib -lQt5Widgets -lQt5Gui -lQt5Core -lGL -lpthread
#LIBS += -L/usr/lib/i386-linux-gnu/qtcreator/ -lAggregation
