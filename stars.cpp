#include "stars.h"

Stars::Stars() {
    QPen penWhite(Qt::white); // Задаём чёрную кисть
    setPen(penWhite);
    setRect(0, 0, 1, 5);

    //set random position
    int random_number = rand() % (540);
    setPos(random_number, 0 - 200);

    // connect
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));

    timer->start(20);
}

void Stars::move() {
    setPos(x(), y() + 50);
    if (pos().y() - rect().height() > scene()->height()) {
        scene()->removeItem(this);
        delete this;
    }
}
