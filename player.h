#ifndef PLAYER_H
#define PLAYER_H

#include <QGraphicsPixmapItem>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QObject>
#include <bullet.h>
#include <enemy.h>
#include <stars.h>
//#include <game.h>
// #include <QMediaPlayer>


class Player: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Player(QGraphicsPixmapItem *parent = nullptr);
    void keyPressEvent(QKeyEvent *event);
    // QMediaPlayer *bulletsound;

public slots:
    void spawn();
    void make_stars();

private:
    int time;
};

#endif // PLAYER_H
