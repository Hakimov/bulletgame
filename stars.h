#ifndef STARS_H
#define STARS_H

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QTimer>
#include <QDebug>

class Stars : public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    explicit Stars();

signals:

public slots:
    void move();
};

#endif // STARS_H
